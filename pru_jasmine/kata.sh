#!/bin/bash
# Based on https://code.tutsplus.com/tutorials/writing-a-shell-script-from-scratch--net-31426

function help () {
    echo "kata - A simple script that makes using the Jasmine testing framework in a standalone project a little simpler."
    echo ""
    echo "    kata new <FunctionName>     - creates ./FunctionName/FunctionName.js ./FunctionName/FunctionNameSpec.js";
    echo "    kata run                    - runs tests in browser";
}



function copyFilesProject () {
  echo "Copying files..."
  mkdir $1/
  cp ./SpecRunner.html ./$1/
  #cp -rf ./lib ./$1/
  #mkdir ./$1/spec/
  #mkdir ./$1/src/
}

if [ $1 ]
then
  case "$1" in
  new)
    if [ $2 ]
    then
      copyFilesProject $2;
      printf "Creating .js files\n"

      printf "const $2 = function () {\n\n\treturn {};\n\n};" > ./$2/$2.js
      printf "describe('$2', function () {\n\n\tit('', function () {\n\t\texpect().toEqual();\n\t});\n\n});" > ./$2/$2Spec.js

      printf "Modifying SpecRunner.html file\n"

      sed -i -e 's/="lib/="..\/lib/g' ./$2/SpecRunner.html

      sed -i '15,16 d' ./$2/SpecRunner.html
      sed -i '17,18 d' ./$2/SpecRunner.html
      sed -i "15 s|^|<script src='$2.js'></script>|" ./$2/SpecRunner.html
      sed -i "17 s|^|<script src='$2Spec.js'></script>|" ./$2/SpecRunner.html

      cd ./$2

      printf "Created:\n"
      printf "\t- /$2/$2.js\n"
      printf "\t- /$2/$2Spec.js\n"
      printf "Edited:\n"
      printf "\t- /$2/SpecRunner.html\n"

      if [ "`which xdg-open`" = '/usr/bin/xdg-open' ]
      then
        xdg-open ./SpecRunner.html
      else
        echo "Please open SpecRunner.html in your browser"
      fi
    else
      printf 'please add a name for the file\n'
    fi
  ;;
  run)
  if [ "`which xdg-open`" = '/usr/bin/xdg-open' ]
  then
    xdg-open ./SpecRunner.html
  else
    echo "Please open SpecRunner.html in your browser"
  fi
  ;;
  *)
    help;
  ;;
  esac
else
  help;
fi
