// http://www.peterprovost.org/blog/2012/05/02/kata-the-only-way-to-learn-tdd/

describe("String Calculator Kata", function() {
	var st;
	it("empty string returns zero", function() {
		st="";
		expect(stCalc(st)).toEqual(0);
	});

	it("single number return the value", function() {
		st = "3";
		expect(stCalc(st)).toEqual(3);
	});

	it("two numbers, comma delimited, returns the sum", function() {
		st = "5,4";
		expect(stCalc(st)).toEqual(9);
	});	

	it("two numbers, newline delimited, returns the sum", function() {
		st = "3\n4";
		expect(stCalc(st)).toEqual(7);
	});	

	it("three numbers, delimited either way, returns the sum", function() {
		st = "3,4\n5";
		expect(stCalc(st)).toEqual(12);
	});	
});




function stCalc(st){
	var result=0;
	var array=st;

	if (st !== "") {
		if (st.indexOf(",")>0) {
			array = st.split(",");
		}
		if (st.indexOf("\n")>0){
			array = st.split("\n");
		}
		for (i=0; i<array.length;i++){
			//console.log(array[i]);
			result += parseInt(array[i]);
			//console.log(result);
		}
	}
	return result;
}