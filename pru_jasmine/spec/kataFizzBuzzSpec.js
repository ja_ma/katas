describe("KATA FIZZ BUZZ", function() {
	var st;

	it("1 returns 1", function() {
		st="1";
		expect(FizzBuzz.check(st)).toEqual("1");
	});
	it("2 returns 2", function() {
		st="2";
		expect(FizzBuzz.check(st)).toEqual("2");
	});
	it("3 returns Fizz", function() {
		st="3";
		expect(FizzBuzz.check(st)).toEqual("Fizz");
	});
	it("4 returns 4", function() {
		st="4";
		expect(FizzBuzz.check(st)).toEqual("4");
	});	
	it("5 returns Buzz", function() {
		st="5";
		expect(FizzBuzz.check(st)).toEqual("Buzz");
	});		
	it("6 returns Fizz", function() {
		st="6";
		expect(FizzBuzz.check(st)).toEqual("Fizz");
	});		
	it("7 returns 7", function() {
		st="7";
		expect(FizzBuzz.check(st)).toEqual("7");
	});		
	it("8 returns 8", function() {
		st="8";
		expect(FizzBuzz.check(st)).toEqual("8");
	});		
	it("9 returns Fizz", function() {
		st="9";
		expect(FizzBuzz.check(st)).toEqual("Fizz");
	});	
	it("10 returns Buzz", function() {
		st="10";
		expect(FizzBuzz.check(st)).toEqual("Buzz");
	});				
	it("15 returns FizzBuzz", function() {
		st="15";
		expect(FizzBuzz.check(st)).toEqual("FizzBuzz");
	});		
});

describe("KATA JARDIN NIÑOS", function() {
	var st;

	it("a returns 1", function() {
		st="a";
		expect(JardinNinos.countVowels(st)).toEqual(1);
	});
	it("aa returns 2", function() {
		st="aa";
		expect(JardinNinos.countVowels(st)).toEqual(2);
	});	
	it("ee returns 2", function() {
		st="ee";
		expect(JardinNinos.countVowels(st)).toEqual(2);
	});		
	it("e e returns 2", function() {
		st="e e";
		expect(JardinNinos.countVowels(st)).toEqual(2);
	});		
	it("e o u returns 3", function() {
		st="e o u";
		expect(JardinNinos.countVowels(st)).toEqual(3);
	});	
	it("e O u returns 3", function() {
		st="e O u";
		expect(JardinNinos.countVowels(st)).toEqual(3);
	});	
	it("camarero con bandeja returns 8", function() {
		st="camarero conbandeja";
		expect(JardinNinos.countVowels(st)).toEqual(8);
	});		
});