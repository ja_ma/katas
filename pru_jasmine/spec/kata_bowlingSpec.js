/*
https://technologyconversations.com/2014/02/19/scala-tutorial-through-katas-bowling-game-medium/
https://en.wikipedia.org/wiki/Ten-pin_bowling#Scoring
http://slocums.homestead.com/gamescore.html
*/

/*
knocked	golpeado
roll		tirada
frame		conjunto de dos tiradas
strike	X 10 bolos en la primera tirada del frame
spare		/ 10 bolos entre las dos tiradas del frame (en una o dos tiradas)
*/

describe("KATA 20170503 - Bowling", function() {
	var st;
	/*
	describe("Kata", function() {
	it("", function() {
	actual = "";
	expected = "";
	expect(actual).toEqual(expected);
});
*/
describe("Kata", function() {
	it("0 pins in first roll of first frame gives 0 points", function() {
		actual = app.roll(1,1,0);
		expected = 0;
		expect(actual).toEqual(expected);
	});

	it("1 pins in first roll of first frame gives 1 points", function() {
		actual = app.roll(1,1,1);
		expected = 1;
		expect(actual).toEqual(expected);
	});

	it("10 pins in first roll of first frame gives 'strike' (and may add next two rolls)", function() {
		actual = app.roll(1,1,10);
		expected = "strike";
		expect(actual).toEqual(expected);
	});

	it("10 pins in second roll of first frame gives 'spare' (and may add next roll)", function() {
		actual = app.roll(1,2,10);
		expected = "spare";
		expect(actual).toEqual(expected);
	});


	it("1 pins in first roll of first frame, and 3 pins in second roll of first frame, gives 4 points", function() {
		app.roll(1,1,1);
		app.roll(1,2,3);
		actual = app.score(1);
		expected = 4;
		expect(actual).toEqual(expected);
	});

	it("4 pins in first roll of first frame, and 3 pins in second roll of first frame, gives 7 points", function() {
		app.roll(1,1,4);
		app.roll(1,2,3);
		actual = app.score(1);
		expected = 7;
		expect(actual).toEqual(expected);
	});

	it("6 pins in first roll of second frame, and 3 pins in second roll of first frame, gives 9 points", function() {
		app.roll(2,1,6);
		app.roll(2,2,3);
		actual = app.score(2);
		expected = 9;
		expect(actual).toEqual(expected);
	});




});

});
