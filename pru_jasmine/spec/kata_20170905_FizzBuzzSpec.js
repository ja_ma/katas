describe("KATA FizzBuzz", function() {
	var st;

	it("Given a number not divisible neither by 3 nor 5 gets the same number", function() {
		actual = FizzBuzz.checkValue(2);
		expected = 2;
		expect(actual).toEqual(expected);

		actual = FizzBuzz.checkValue(7);
		expected = 7;
		expect(actual).toEqual(expected);
	});

	it("Given a number divisible by 3 gets 'Fizz'", function() {
		actual = FizzBuzz.checkValue(3);
		expected = 'Fizz';
		expect(actual).toEqual(expected);
	});

	it("Given a number divisible by 5 gets 'Buzz'", function() {
		actual = FizzBuzz.checkValue(10);
		expected = 'Buzz';
		expect(actual).toEqual(expected);
	});

	it("Given a number divisible by bith 3 and 5 gets 'FizzBuzz'", function() {
		actual = FizzBuzz.checkValue(30);
		expected = 'FizzBuzz';
		expect(actual).toEqual(expected);
	});

	it("List the FizzBuzz numbers from 1 to 100 contains expected subString", function() {
		actual = FizzBuzz.listNumbers();
		expected = 'FizzBuzz';
		subString = '1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16';
		expect(actual.indexOf(subString) !== -1).toBe(true);
		subString = '67 68 Fizz Buzz 71 Fizz 73 74 FizzBuzz 76 77 Fizz 79 Buzz';
		expect(actual.indexOf(subString) !== -1).toBe(true);
	});



});
