describe("FizzBuzz", function() {

	it("lists numbers from 1 to 100", function() {
		list = FizzBuzz.populateList();

		expect(list.length).toEqual(100);
		expect(list[3]).toEqual(4);
		expect(list[97]).toEqual(98);
	});

	it("populates list with 'Fizz' if divisible by 3", function() {
		list = FizzBuzz.populateList();

		expect(list[5]).toEqual('Fizz');
		expect(list[8]).toEqual('Fizz');
	});

	it("populates list with 'Buzz' if divisible by 5", function() {
		list = FizzBuzz.populateList();

		expect(list[9]).toEqual('Buzz');
		expect(list[99]).toEqual('Buzz');
	});

	it("populates list with 'FizzBuzz' if divisible by 3 and 5", function() {
		list = FizzBuzz.populateList();

		expect(list[14]).toEqual('FizzBuzz');
	});

});
