// http://tip.dis.ulpgc.es/numeros-texto/

describe("KATA NUMERO A LETRAS", function() {
	var st;
	describe("Unidades", function() {
		it("1 is uno", function() {
			actual = numberToletter.convert(1);
			expected = "uno";
			expect(actual).toEqual(expected);
		});

		it("2 is dos", function() {
			actual = numberToletter.convert(2);
			expected = "dos";
			expect(actual).toEqual(expected);
		});

		it("3 is tres", function() {
			actual = numberToletter.convert(3);
			expected = "tres";
			expect(actual).toEqual(expected);
		});
		it("9 is nueve", function() {
			actual = numberToletter.convert(9);
			expected = "nueve";
			expect(actual).toEqual(expected);
		});
	});

	describe("Decenas", function() {
		it("10 is diez", function() {
			actual = numberToletter.convert(10);
			expected = "diez";
			expect(actual).toEqual(expected);
		});
		it("20 is veinte", function() {
			actual = numberToletter.convert(20);
			expected = "veinte";
			expect(actual).toEqual(expected);
		});
		it("60 is sesenta", function() {
			actual = numberToletter.convert(60);
			expected = "sesenta";
			expect(actual).toEqual(expected);
		});
		it("90 is noventa", function() {
			actual = numberToletter.convert(90);
			expected = "noventa";
			expect(actual).toEqual(expected);
		});
	});

	describe("De 11 a 19", function() {
		it("11 is once", function() {
			actual = numberToletter.convert(11);
			expected = "once";
			expect(actual).toEqual(expected);
		});
		it("14 is catorce", function() {
			actual = numberToletter.convert(14);
			expected = "catorce";
			expect(actual).toEqual(expected);
		});
		it("18 is dieciocho", function() {
			actual = numberToletter.convert(18);
			expected = "dieciocho";
			expect(actual).toEqual(expected);
		});
	});

	describe("De 21 a 29", function() {
		it("21 is veintiuno", function() {
			actual = numberToletter.convert(21);
			expected = "veintiuno";
			expect(actual).toEqual(expected);
		});
		it("23 is veintitres", function() {
			actual = numberToletter.convert(23);
			expected = "veintitres";
			expect(actual).toEqual(expected);
		});
		it("27 is veintisiete", function() {
			actual = numberToletter.convert(27);
			expected = "veintisiete";
			expect(actual).toEqual(expected);
		});
	});

	describe("De 31 a 39", function() {
		it("31 is treinta y uno", function() {
			actual = numberToletter.convert(31);
			expected = "treinta y uno";
			expect(actual).toEqual(expected);
		});
		it("33 is treinta y tres", function() {
			actual = numberToletter.convert(33);
			expected = "treinta y tres";
			expect(actual).toEqual(expected);
		});
		it("37 is treinta y siete", function() {
			actual = numberToletter.convert(37);
			expected = "treinta y siete";
			expect(actual).toEqual(expected);
		});
	});

	describe("40s, 50s, 60s, 70s, 80s, 90s", function() {
		it("41 is cuarenta y uno", function() {
			actual = numberToletter.convert(41);
			expected = "cuarenta y uno";
			expect(actual).toEqual(expected);
		});
		it("53 is cincuenta y tres", function() {
			actual = numberToletter.convert(53);
			expected = "cincuenta y tres";
			expect(actual).toEqual(expected);
		});
		it("67 is sesenta y siete", function() {
			actual = numberToletter.convert(67);
			expected = "sesenta y siete";
			expect(actual).toEqual(expected);
		});
		it("72 is setenta y dos", function() {
			actual = numberToletter.convert(72);
			expected = "setenta y dos";
			expect(actual).toEqual(expected);
		});
		it("85 is ochenta y cinco", function() {
			actual = numberToletter.convert(85);
			expected = "ochenta y cinco";
			expect(actual).toEqual(expected);
		});
		it("94 is noventa y cuatro", function() {
			actual = numberToletter.convert(94);
			expected = "noventa y cuatro";
			expect(actual).toEqual(expected);
		});
		it("99 is noventa y nueve", function() {
			actual = numberToletter.convert(99);
			expected = "noventa y nueve";
			expect(actual).toEqual(expected);
		});
	});
	describe("100s", function() {
		it("100 is cien", function() {
			actual = numberToletter.convert(100);
			expected = "cien";
			expect(actual).toEqual(expected);
		});
		it("101 is ciento uno", function() {
			actual = numberToletter.convert(101);
			expected = "ciento uno";
			expect(actual).toEqual(expected);
		});
		it("111 is ciento once", function() {
			actual = numberToletter.convert(111);
			expected = "ciento once";
			expect(actual).toEqual(expected);
		});
		it("147 is ciento cuarenta y siete", function() {
			actual = numberToletter.convert(147);
			expected = "ciento cuarenta y siete";
			expect(actual).toEqual(expected);
		});
		it("189 is ciento ochenta y nueve", function() {
			actual = numberToletter.convert(189);
			expected = "ciento ochenta y nueve";
			expect(actual).toEqual(expected);
		});
		it("199 is ciento noventa y nueve", function() {
			actual = numberToletter.convert(199);
			expected = "ciento noventa y nueve";
			expect(actual).toEqual(expected);
		});
	});

});

var numberToletter = {
	//TODO: Tildes
	convert: function(number){
		units = ["uno","dos","tres","cuatro","cinco","seis","siete","ocho","nueve"];
		tens = ["diez","veinte","treinta","cuarenta","cincuenta","sesenta","setenta","ochenta","noventa","cien"];
		onceAdiecinueve = ["once","doce","trece","catorce","quince","dieciséis","diecisiete","dieciocho","diecinueve"];

		switch (true) {
			case (number <10):
			result = units[number-1];
			break;
			case (number%10 === 0):
			var decena = number/10;
			result = tens[decena-1];
			break;
			case (number > 10 && number < 20):
			result = onceAdiecinueve[number-11];
			break;
			case (number > 20 && number < 30):
			unidades = number - 20;
			result = "veinti" + units[unidades-1];
			break;
			case (number > 30 && number < 100):
			decena = Math.floor(number/10);
			unidades = number - decena*10;
			result = tens[decena-1] + " y " + units[unidades - 1];
			break;
			case (number > 100 && number < 200):
			cientos = "ciento ";
			number = number - 100;
			switch (true) {
				case (number <10):
				result = cientos + units[number-1];
				break;
				case (number > 10 && number < 20):
				result = cientos + onceAdiecinueve[number-11];
				break;
				case (number > 20 && number < 30):
				unidades = number - 20;
				result = cientos + "veinti"+units[unidades-1];
				break;
				case (number > 30 && number < 100):
				decena = Math.floor(number/10);
				unidades = number - decena*10;
				result = cientos + tens[decena-1] + " y " + units[unidades - 1];
				break;
				default:

			}
			break;
			default:
			result = number;

		}
		return result;
	}
};
