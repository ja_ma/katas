//https://www.guru99.com/test-driven-development.html#7
describe("KATA Password", function() {

	it("Password length < 5 returns false", function() {
		const pass = PasswordValidator();

		actual = pass.isValid("0123");
		expected = false;
		expect(actual).toBe(expected);
	});

	it("Password lentgh > 10 returns false", function() {
		const pass = PasswordValidator();

		actual = pass.isValid("012345678901234");
		expected = false;
		expect(actual).toBe(expected);
	});

	it("Password length between 5 and 10 returns true", function() {
		const pass = PasswordValidator();

		actual = pass.isValid("0123456");
		expected = true;
		expect(actual).toBe(expected);
	});
	
});
