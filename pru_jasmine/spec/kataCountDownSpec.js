// Ver: https://gist.github.com/ramingar/138f69451e5233042a503edf230012c8


describe("KATA COUNTDOWN", function() {
	var st;

	it("Passing 9 we get 8 after 1 unit time", function() {
		actual = CountDown.getResult(9,1);
		expected = 8;
		expect(actual).toEqual(expected);
	});

	it("Passing 8 we get 3 after 5 unit time", function() {
		actual = CountDown.getResult(8,5);
		expected = 3;
		expect(actual).toEqual(expected);
	});



});

var CountDown = {
	getResult: function(initial,period){
		return initial-period;
	}
}
