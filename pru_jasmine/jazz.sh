#!/bin/bash
# Based on https://code.tutsplus.com/tutorials/writing-a-shell-script-from-scratch--net-31426

function help () {
    echo "jazz - A simple script that makes using the Jasmine testing framework in a standalone project a little simpler."
    echo ""
    #echo "    jazz init                  - include jasmine in the project";
    echo "    jazz create <FunctionName>  - creates ./src/FunctionName.js ./spec/FunctionNameSpec.js";
    echo "    jazz new <FunctionName>     - creates ./FunctionName/src/FunctionName.js ./FunctionName/spec/FunctionNameSpec.js";
    echo "    jazz run                    - runs tests in browser";
}

function copyFiles () {
  echo "Copying files..."
  cp ../SpecRunner.html .
  cp -rf ../lib/ .
  mkdir spec/
  mkdir src/
}

function copyFilesProject () {
  echo "Copying files..."
  mkdir $1/
  cp ./SpecRunner.html ./$1/
  cp -rf ./lib ./$1/
  mkdir ./$1/spec/
  mkdir ./$1/src/
}

#JASMIME_LINK="http://cloud.github.com/downloads/pivotal/jasmine/jasmine-standalone-1.3.1.zip"
JASMIME_LINK="https://github.com/jasmine/jasmine/releases/download/v2.8.0/jasmine-standalone-2.8.0.zip"

if [ $1 ]
then
  case "$1" in
  init)
    echo "Downloading Jasmine . . ."
    curl -sO $JASMIME_LINK
    unzip -q `basename $JASMIME_LINK`
    rm `basename $JASMIME_LINK` src/*.js spec/*.js

    if which xattr > /dev/null && [ "`xattr SpecRunner.html`" = "com.apple.quarantine" ]
    then
      xattr -d com.apple.quarantine SpecRunner.html
    fi

    sed -i "" "12,18d" SpecRunner.html
    echo "Jasmine initialized!"
  ;;
  create)
    if [ $2 ]
    then
      copyFiles $2;

      printf "const $2 = function () {\n\n\treturn {};\n\n};" > ./src/$2.js
      printf "describe('$2', function () {\n\n\tit('', function () {\n\t\texpect().toEqual();\n\t});\n\n});" > ./spec/$2Spec.js

      sed -i '15,16 d' SpecRunner.html
      sed -i '17,18 d' SpecRunner.html
      sed -i "15 s|^|<script src='src/$2.js'></script>|" SpecRunner.html
      sed -i "17 s|^|<script src='spec/$2Spec.js'></script>|" SpecRunner.html

      printf "Created:\n"
      printf "\t- src/$2.js\n"
      printf "\t- spec/$2Spec.js\n"
      printf "Edited:\n"
      printf "\t- SpecRunner.html\n"
    else
      printf 'please add a name for the file\n'
    fi
  ;;
  new)
    if [ $2 ]
    then
      copyFilesProject $2;

      printf "const $2 = function () {\n\n\treturn {};\n\n};" > ./$2/src/$2.js
      printf "describe('$2', function () {\n\n\tit('', function () {\n\t\texpect().toEqual();\n\t});\n\n});" > ./$2/spec/$2Spec.js

      sed -i '15,16 d' ./$2/SpecRunner.html
      sed -i '17,18 d' ./$2/SpecRunner.html
      sed -i "15 s|^|<script src='src/$2.js'></script>|" ./$2/SpecRunner.html
      sed -i "17 s|^|<script src='spec/$2Spec.js'></script>|" ./$2/SpecRunner.html

      cd ./$2

      printf "Created:\n"
      printf "\t- src/$2.js\n"
      printf "\t- spec/$2Spec.js\n"
      printf "Edited:\n"
      printf "\t- SpecRunner.html\n"
    else
      printf 'please add a name for the file\n'
    fi
  ;;
  "run")
    #if [ "`which open`" = '/usr/bin/open' ]
    if [ "`which xdg-open`" = '/usr/bin/xdg-open' ]
    then
      xdg-open ./SpecRunner.html
    else
      echo "Please open SpecRunner.html in your browser"
    fi
  ;;
  *)
    help;
  ;;
  esac
else
  help;
fi
