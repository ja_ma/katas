const PasswordValidator = function() {
	const MIN_LEN = 5;
	const MAX_LEN = 10;

	const isValid = function(pass){
		var result = false;
		if (MIN_LEN <= pass.length && MAX_LEN >= pass.length) { result = true; }

		return result;
	};

	const andCallFake = function (key){
		console.log(localStorage.getItem(key));
	};

	return {isValid, andCallFake};
};
