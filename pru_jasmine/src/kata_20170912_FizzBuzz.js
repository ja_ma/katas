const FizzBuzz = {
  INITIAL_POSITION: 1,
  FINAL_POSITION: 100,
  FIZZ: 'Fizz',
  BUZZ: 'Buzz',
  FIZZBUZZ: 'FizzBuzz',
  FIZZ_VALUE: 3,
  BUZZ_VALUE: 5,
  FIZZBUZZ_VALUE: 15,
  list: [],
  map: new Map(),

  populatemap: function() {
    this.map.set(this.FIZZ_VALUE, this.FIZZ);
    this.map.set(this.BUZZ_VALUE, this.BUZZ);
    this.map.set(this.FIZZBUZZ_VALUE, this.FIZZBUZZ);
  },

  populateList: function(){
    this.populatemap();

    for (let element = this.INITIAL_POSITION; element <= this.FINAL_POSITION; element++){
      elementToAdd = element;
      for (var [key, value] of this.map.entries()) {
        if(this.isDivisibleBy(element, key)) { elementToAdd = value; }
      }
      this.list.push(elementToAdd);
    }
    return this.list;
  },

  isDivisibleBy: function(element, divisor) {
    return element % divisor == 0;
  },
};
