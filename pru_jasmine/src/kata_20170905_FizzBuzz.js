var FizzBuzz = {
  min_val: 1,
  max_val: 100,

  checkValue: function(valueToCheck) {
    let result = valueToCheck;
    if (this.isDivisible(valueToCheck, 3)) {
      result = "Fizz";
    }
    if (this.isDivisible(valueToCheck, 5)) {
      result = "Buzz";
    }
    if (this.isDivisible(valueToCheck, 3) && this.isDivisible(valueToCheck, 5)) {
      result = "FizzBuzz";
    }

    return result;
  },

  listNumbers: function() {
    let listNumbers = "";

    for (let counter = this.min_val; counter <= this.max_val; counter++){
      listNumbers = listNumbers + this.checkValue(counter) + " ";
    }
    console.log(listNumbers);
    return listNumbers;
  },

  isDivisible: function(number, divisor) {
    let divisible =  false;
    if (0 == (number % divisor)) {
      divisible = true;
    }
    return divisible;
  }

};
