var FizzBuzz = {
	
	check: function (val){
		var result = val;
		if (val % 3 == 0) {result = "Fizz";}
		if (val % 5 == 0) {result = "Buzz";}
		if (val % 3 == 0 && val % 5 == 0) {result = "FizzBuzz";}
		return result;
	},

	listAll: function (){
		for (num = 1; num <= 100; num++){
			console.log(this.check(num));
		}
	}
}

FizzBuzz.listAll();